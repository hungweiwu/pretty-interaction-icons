import React, { Component } from "react";

class HeartIcon extends Component {
  render() {
    return (
      <svg viewBox="2 3 30 26" {...this.props}>
        <path d="M16,28.261c0,0-14-7.926-14-17.046c0-9.356,13.159-10.399,14-0.454c1.011-9.938,14-8.903,14,0.454
          C30,20.335,16,28.261,16,28.261z" />
      </svg>
    );
  }
}

export default HeartIcon;
