import MediumClap from "./components/MediumClap";
import Like from "./components/Like";
import Music from "./components/Music";
import Heart from "./components/Heart";

module.exports = {
  Heart,
  Music,
  Like,
  MediumClap
};
