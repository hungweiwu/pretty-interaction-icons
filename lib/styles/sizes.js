module.exports = {
  // square sizes on 8pt grid system
  square: {
    extraSmall: "24px",
    small: "32px",
    medium: "40px",
    large: "48px",
    extraLarge: "56px"
  }
};
