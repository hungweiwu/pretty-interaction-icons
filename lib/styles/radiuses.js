// radius {from: to} definitions for mojs
module.exports = {
  extraSmall: {
    0: 30
  },
  small: {
    10: 50
  },
  medium: {
    30: 60
  },
  large: {
    50: 80
  },
  extraLarge: {
    70: 100
  }
};
