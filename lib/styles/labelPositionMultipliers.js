// multipliers to determine where the label is positioned
module.exports = {
  top: {
    top: -1 / 2,
    left: 1 / 2
  },
  bottom: {
    top: 3 / 2,
    left: 1 / 2
  },
  left: {
    top: 2 / 5,
    left: -1 / 2
  },
  right: {
    top: 2 / 5,
    left: 3 / 2
  }
};
