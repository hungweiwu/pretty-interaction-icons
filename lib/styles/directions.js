// burst direction definitions for mojs
// angle offset from north, degree of burst
module.exports = {
  top: {
    angle: -30,
    degree: 60
  },
  right: {
    angle: 60,
    degree: 60
  },
  bottom: {
    angle: 150,
    degree: 60
  },
  left: {
    angle: 240,
    degree: 60
  },
  all: {
    angle: 0,
    degree: 360
  }
};
