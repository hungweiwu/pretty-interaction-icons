// point count definition for shapes in mojs
module.exports = {
  circle: 0,
  triangle: 3,
  square: 4,
  pentagon: 5,
  hexagon: 6,
  septagon: 7,
  zigzag: 5
};
